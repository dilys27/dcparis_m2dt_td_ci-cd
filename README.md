# dcparis_m2dt_td_ci-cd

exemple d'url avec paramètres :
server/job/myjob/buildWithParameters?token=TOKEN&PARAMETER=Value

toutes les 2 minutes :
*/2 * * * *

tous les soirs à 23h45 :
45 23 * * *

du lundi au vendredi à 2h13 :
13 2 * * 1-5

Ressources :
- https://crontab.guru/#13_2_*_*_1-5
- https://github.com/jenkinsci/jenkins/blob/master/core/src/main/resources/hudson/triggers/TimerTrigger/help-spec.jelly
