from datetime import datetime

def get_time_of_day(hour):
	"""
	Retourne une chaine de caractere correspondant a la periode de la journée à partir de l'heure donnée en paramètre ( 0 - 23 )
    :param hour: L'heure de la journée à rechercher
    :type hour: int
    :return: Le résultat de la recherche parmi les différentes options
    :rtype: string

    Commentaire :
    Test unitaire sur la fonction précédente de base est non déterministe car le résultat va dépendre de l'heure d'execution de la fonction.
	Pas simple à tester (utilise des dépendences comme l'heure du système, utiliser des setUp et des tearDown complexes, manipulation peut rendre le test failed).

    """

	if (hour >= 0 and hour < 6):
		return "Bonne nuit"
	if (hour >= 6 and hour < 12):
		return "Cafe du matin"
	if (hour >= 12 and hour < 18):
		return "Bon apres midi"

	return "Bonne soiree"

now = int(datetime.now().strftime("%H"))
print (get_time_of_day(now))
