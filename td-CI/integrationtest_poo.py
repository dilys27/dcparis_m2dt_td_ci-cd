import unittest

import poo

personne = poo.Personne("Harry", "Potter")

class TestSum(unittest.TestCase):

    def test_creer_compte_bancaire(self):
        """
        Vérifie que le compte bancaire est bien créé
        """
        personne.creer_compte_bancaire()
        solde = personne.compte_bancaire.solde
        self.assertEqual(solde, 0)

    def test_depot(self):
        """
        Vérifie que le dépot d'une somme fonctionne correctement
        """
        personne.depot(1000)
        solde = personne.compte_bancaire.solde
        self.assertEqual(solde, 1000)

    def test_retrait(self):
        """
        Vérifie que le retrait d'une somme fonctionne correctement
        """
        personne.retrait(500)
        solde = personne.compte_bancaire.solde
        self.assertEqual(solde, 500)

if __name__ == '__main__':
    unittest.main()
