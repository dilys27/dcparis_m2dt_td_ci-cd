def add(x,y):
    """
    Ajoute deux nombres l'un à l'autre et retourne le résultat.

    :param x: Le premier nombre à ajouter
    :param y: Le second nombre à ajouter
    :type x: int, float
    :type y: int, float
    :return: Le résultat de l'addition
    :rtype: int, float

    """
    return x+y

def substract(x,y):
    """
    Soustrait deux nombres l'un à l'autre et retourne le résultat.

    :param x: Le premier nombre à soustraire
    :param y: Le second nombre à soustraire
    :type x: int, float
    :type y: int, float
    :return: Le résultat de la soustraction
    :rtype: int, float

    """
    return x-y

def multiply(x,y):
    """
    Multiplie deux nombres entre eux et retourne le résultat.

    :param x: Le premier nombre à multiplier
    :param y: Le second nombre à multiplier
    :type x: int, float
    :type y: int, float
    :return: Le résultat de la multiplication
    :rtype: int, float

    """
    return x*y

def divide(x,y):
    if y == 0:
        raise ValueError('division par O impossible')
    else:
        """
        Divise deux nombres entre eux et retourne le résultat.

        :param x: Le premier nombre à diviser (numérateur ou dividende)
        :param y: Le second nombre à diviser (dénominateur ou diviseur)
        :type x: int, float
        :type y: int, float
        :return: Le résultat de la division
        :rtype: int, float

        """
        return x/y
